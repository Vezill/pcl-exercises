(in-package #:cl-user)

(defpackage #:unit21.id3-parser
  (:use #:common-lisp
        #:alexandria
        #:unit21.binary-data
        #:unit21.pathnames)
  (:export #:read-id3
           #:mp3-p
           #:id3-p
           #:album
           #:composer
           #:genre
           #:encoding-program
           #:artist
           #:part-of-set
           #:track
           #:song
           #:year
           #:size
           #:translated-genre))
