(defsystem #:unit21.id3-parser
  :name "id3-parser"
  :version "1.0"
  :description "ID3 Tag parser from PCL chapter 25"
  :depends-on (#:alexandria
               #:unit21.binary-data
               #:unit21.pathnames)
  :serial t
  :components ((:file "package")
               (:file "id3-parser")))
