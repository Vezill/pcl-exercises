(defsystem #:unit21.pathnames
  :name "pathnames"
  :version "1.0"
  :description "Pathnames library from PCL chapter 15"
  :serial t
  :components ((:file "package")
               (:file "pathnames")))
