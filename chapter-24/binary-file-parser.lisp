;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Practical Common Lisp - Chapter 24 - Practical: Parsing Binary Files
;;;; Date: 30 January 2018
;;;; Author: Nick Donais (nick.donais@protonmail.com)
;;;; Descripiton: Examples of how to code certain read and write methods and object representations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :cl-user)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :alexandria))

(defpackage :binary-parser
  (:use :cl :alexandria))

(in-package :binary-parser)
(load "../chapter-15/pathnames.lisp")


(defconstant +null+ (code-char 0))


;; read-u2 can also be written as:
;; (defun read-u2 (in)
;;   (logior
;;    (ash (read-byte in) 8)
;;    (ash (read-byte in) 0)))  ; The second ash is not required, more for style purposes
;; which uses bit shifting and logical inclusive or

(defun read-u2 (in)
  (let ((u2 0))
    (setf (ldb (byte 8 8) u2) (read-byte in))
    (setf (ldb (byte 8 0) u2) (read-byte in))
    u2))

(defun write-u2 (out value)
  (write-byte (ldb (byte 8 8) value) out)
  (write-byte (ldb (byte 8 0) value) out))

(defun read-null-terminated-ascii (in)
  (with-output-to-string (s)
    (loop :for char := (code-char (read-byte in))
          :until (char= char +null+) :do (write-char char s))))

(defun write-null-terminated-ascii (string out)
  (loop :for char :across string :do
    (write-byte (char-code char) out))
  (write-byte (char-code +null+) out))


(defclass id3-tag ()
  ((identifier
    :initarg :identifier :accessor identifier)
   (major-version
    :initarg :major-version :accessor major-version)
   (revision
    :initarg :revision :accessor revision)
   (flags
    :initarg :flags :accessor flags)
   (size
    :initarg :size :accessor size)
   (frames
    :initarg :frames :accessor frames)))

(defun read-id3-tag (in)
  (let ((tag (make-instance 'id3-tag)))
    (with-slots (identifier major-version revision flags size frames) tag
      (setf identifier (read-iso-8859-1-string in :length 3))
      (setf major-version (read-u1 in))
      (setf revision (read-u1 in))
      (setf flags (read-u1 in))
      (setf size (read-id3-encoded-size in))
      (setf frames (read-id3-frames in :tag-size size)))
    tag))


(define-binary-class id3-tag
  ((file-identifier (iso-8859-1-string :length 3))
   (major-version u1)
   (revision u1)
   (flags u1)
   (size id3-tag-size)
   (frames (id3-frames :tag-size size))))
