(defsystem #:unit21.binary-data
  :name "binary-data"
  :version "1.0"
  :description "Binary data parsing library from PCL chapter 24"
  :depends-on (#:alexandria
               #:trivial-indent)
  :serial t
  :components ((:file "package")
               (:file "binary-data")))
