(in-package #:cl-user)

(defpackage #:unit21.binary-data
  (:use #:cl
        #:alexandria
        #:trivial-indent)
  (:export #:+null+
           #:*in-progress-objects*
           #:define-binary-class
           #:define-tagged-binary-class
           #:define-binary-type
           #:read-value
           #:write-value
           #:current-binary-object
           #:parent-of-type))
