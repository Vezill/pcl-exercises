;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Practical Common Lisp - Chapter 03 - Practical: A Simple Database
;;;; Date: 09 December 2017
;;;; Author: Nick Donais (nick.donais@protonmail.com)
;;;; Description: Decided to follow along with the exercise in ch3 but change the target from a cd
;;;;   collection to a board game collection.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *database* nil)

(defun dump-database ()
  (dolist (row *database*)
    (format t "~{~A:~12T~A~%~}~%" row)))

(defun make-board-game (title designer publisher rating played)
  (list :title title :designer designer :publisher publisher :rating rating :played played))

(defun add-record (row)
  (push row *database*))

(defun prompt-read (prompt)
  (format *query-io* "~A: " prompt)
  (force-output *query-io*) ; apparently some impls don't force output without a newline
  (read-line *query-io*))

(defun parse-float (line)
  "Just a basic string to float parses found on stack overflow as board game ratings are in floats
   not integers."
  (with-input-from-string (s line)
    (read s nil nil)))

(defun prompt-for-board-game ()
  (make-board-game
   (prompt-read "Title")
   (prompt-read "Designer")
   (prompt-read "Publisher")
   (or (parse-float (prompt-read "Rating")) 0)
   (y-or-n-p "Played?")))

(defun add-board-games ()
  (loop (add-record (prompt-for-board-game))
        (unless (y-or-n-p "Another?")
          (return))))

(defun save-database (filename)
  (with-open-file (out filename :direction :output :if-exists :supersede)
    (with-standard-io-syntax
      (print *database* out))))

(defun select (selector-fn)
  (remove-if-not selector-fn *database*))

(defun make-comparison-expr (field value)
  `(equal (getf bg ,field) ,value))

(defun make-comparisons-list (fields)
  (loop :while fields
        :collecting (make-comparison-expr (pop fields) (pop fields))))

(defmacro where (&rest clauses)
  `#'(lambda (bg) (and ,@(make-comparisons-list clauses))))

(defun update (selector-fn &key title designer publisher rating (played nil played-p))
  (setf *database* (mapcar
              #'(lambda (row)
                  (when (funcall selector-fn row)
                    (if title (setf (getf row :title) title))
                    (if designer (setf (getf row :designer) designer))
                    (if publisher (setf (getf row :publisher) publisher))
                    (if rating (setf (getf row :rating) rating))
                    (if played-p (setf (getf row :played) played)))
                  row) *database*)))

(defun delete-rows (selector-fn)
  (setf *database* (remove-if selector-fn *database*)))
