;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Practical Common Lisp - Chapter 08 - Macros: Defining Your Own
;;;; Date: 09 December 2017
;;;; Author: Nick Donais (nick.donais@protonmail.com)
;;;; Description: Some useful macros
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop :for n :in names :collect `(,n (make-symbol ,(string n))))
     ,@body))

(defmacro once-only ((&rest names) &body body)
  (let ((gensyms (loop :for n :in names :collect (gensym (string n)))))
    `(let (,@(loop :for g :in gensyms :collect `(,g (gensym))))
       `(let (,,@(loop :for g :in gensyms :for n :in names :collect ``(,,g ,,n)))
          ,(let (,@(loop :for n :in names :for g :in gensyms :collect `(,n ,g)))
             ,@body)))))
